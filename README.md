# Shopee Food order group mini tool!

Welcome to the Shopee Food order group mini tool!

---

## Setup

...

## TODO

1. &copy; Learn how to play with chrome extension 
2. &copy; Init project use stacks: Vuejs, chrome extension, build tool 
3. &copy; Add bootstrap css 
4. &copy; Simple show extension page
5. &copy; Simple work with Shopee food: validate page, add even listener on "Add" button clicked
6. &copy; Simple save data to online storage when "Add" button clicked
7. &copy; Add name user does action to page and storage data
8. Show booked (by date) by admin group
   1. &copy; Admin init today list (manually by hand, no cronjob, try adding later)
   2. &copy; Show today as main content
   3. &copy; Sync current user from popup to tabs content
   4. &copy; Save/load user from localstorage
   5. &copy; Setup "Add" button on scroll more
   6. &copy; Fix "Add" button:
      1. &copy; Avoid re-generate event listener
      2. &copy; Add icon beside the "Add" button to mark registered event listener
      3. &copy; Fix length of name to be sent (max 128)
9. Show booked (by date) by user himself
   1. Refactor content Show for user/admin page
   2. Load more in admin page (current auto limited 10 only!)
   3. ...
10. Deploy
11. more...