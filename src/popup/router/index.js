import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from "../views/Home";

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'Home',
        component: Home
    },
    {
        path: '/user',
        name: 'user-orders',
        component: () => import(/* webpackChunkName: "userOrders" */ '../views/UserOrders')
    },
    {
        path: '/admin-dashboard',
        name: 'AdminDashboard',
        component: () => import(/* webpackChunkName "admin" */'../views/AdminDashboard')
    },
    {
        path: '/about',
        name: 'About',
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
    }
]

const router = new VueRouter({
    base: '/',
    routes
})

export default router
