import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        user: null,
        userType: 'user', // user, admin,
        isTodayInit: false,
    },
    mutations: {
        setUser(state, user) {
            state.user = user;
        },
        setUserType(state, userType) {
            state.userType = userType;
        },
        setTodayInit(state, isInitialized) {
            state.isTodayInit = isInitialized;
        }
    },
    actions: {},
    getters: {
        isUser(state) {
            return state.userType === 'user';
        }
    },
    modules: {}
})
