console.log('background.js...');

console.log('current user: ', window.currentUser);

browser.runtime.onMessage.addListener(function (request, sender, sendResponse) {
    console.log('background received a request', request, sender, sendResponse);

    if (request && request.greeting === "GetURL") {
        let timer = setTimeout(function () {
            console.log('background.js resp on timeout');
            sendResponse({navURL: '', error: 'timeout'});
        }, 10000);

        console.log('background.js clear timeout');
        clearTimeout(timer);

        var tabURL = "Not set yet";
        chrome.tabs.query({active: true, lastFocusedWindow: true}, function (tabs) {
            if (tabs.length === 0) {
                sendResponse({navURL: '', error: 'failed'});
                return;
            }
            tabURL = tabs[0].url;
            sendResponse({navURL: tabURL, error: ''});
        });

        /* very important, need return true to make 'sendResponse' works properly! */
        return true;
    }
})
