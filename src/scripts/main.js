console.log('main.js...');

/* setup button event */
let loadedFoods = [
    // by hash of name__price
];
setTimeout(setupBtnEvent, 5000);
document.addEventListener('scroll', function () {
    setTimeout(setupBtnEvent, 5000);
});

/* get data collection */
const COLLECTION_URI = "https://api.jsonbin.io/v3/c";
const BIN_URI = "https://api.jsonbin.io/v3/b";
const M_KEY = "$2b$10$M15o/iDDMOEqcIXU1aMOyOt8kiAmWNVmQOGMyr6p3.xX.HlTE1oKG";
let collection = null;
getCollection();

/* current user */
let currentUser = '';
console.log('current user: ', currentUser);
chrome.runtime.onMessage.addListener(function (request, sender, sendResponse) {
    console.log('main received a request', request, sender, sendResponse);

    if (request && request.greeting === "updateUser") {
        let timer = setTimeout(function () {
            console.log('main.js resp on timeout');
            sendResponse({navURL: '', error: 'timeout'});
        }, 10000);

        console.log('main.js clear timeout');
        clearTimeout(timer);

        var user = request.data;
        if (user) {
            currentUser = user;
            console.log('new current user: ', currentUser);
            sendResponse({error: ''});
        } else {
            sendResponse({error: 'failed'});
        }

        /* very important, need return true to make 'sendResponse' works properly! */
        return true;
    }
})

/* === local functions === */
function setupBtnEvent() {
    // const addButtons = document.querySelectorAll("#restaurant-item .item-restaurant-row .row .btn-adding");
    const addButtons = document.querySelectorAll("#restaurant-item .btn-adding");
    if (!addButtons) {
        console.log("could not find addButtons");
        return;
    }

    console.log("addButtons: ", addButtons);
    for (let idx in addButtons) {
        if (!isElement(addButtons[idx])) {
            continue;
        }

        /* avoid registered buttons */
        let hash = calcFoodHash(addButtons[idx]);
        if (loadedFoods.includes(hash)) {
            console.log(`addButton: skipped hash ${hash}`, loadedFoods);
            //continue; // page always re-render, so that now temporarily can not skip here :(((
        } else {
            loadedFoods.push(hash);
        }

        console.log("addButton: ", addButtons[idx]);

        /* try removing event first to avoid duplicate calling event */
        let elClone = addButtons[idx].cloneNode(true);
        try {
            addButtons[idx].parentNode.replaceChild(elClone, addButtons[idx]);
        } catch (e) {
            console.log("addButton: remove event error: ", e);
        }

        /* add new event */
        try {
            elClone.addEventListener('click', function () {
                onAddFoodToCart(elClone);
            });
        } catch (e) {
            console.log("addButton: error: ", e);
        }

        /* add marked symbol beside "Add" button */
        let parent = elClone.parentElement;
        let markEl = null;
        if (parent && !parent.querySelector('.of-marked')) {
            console.log("add marked beside Add Button: ", elClone);
            markEl = document.createElement('div');
            markEl.textContent = 'v';
            markEl.classList.add('of-marked');
            markEl.style.cssText = `
                display: inline-block;
                font-size: 20px;
                line-height: 20px;
                width: 22px;
                height: 22px;
                background-color: #76ee2d;
                text-align: center;
                margin-left: 1px;
                text-color: green;
            `;
            parent.appendChild(markEl);
        }
    }
}

function calcFoodHash(el) {
    let parent = el.parentElement.parentElement.parentElement.parentElement;
    if (parent) {
        let nameEl = parent.querySelector('.item-restaurant-info .item-restaurant-name');
        let priceEl = parent.querySelector('.item-restaurant-more .product-price > .current-price');
        let name = encodeURIComponent(nameEl.textContent);
        let price = priceEl.textContent.replace(/[^0-9]+/g, '');
        if (nameEl && priceEl) {
            let hash = hashCode(`${name}__${price}`);
            console.log(`New Food hash: ${hash}`);
            return hash;
        }
    }

    return '';
}

function hashCode(s) {
    let h;
    for (let i = 0; i < s.length; i++)
        h = Math.imul(31, h) + s.charCodeAt(i) | 0;

    return h;
}

function onAddFoodToCart(el) {
    try {
        let parent = el.parentElement.parentElement.parentElement.parentElement;
        if (parent) {
            console.log("parent: ", parent);
            let nameEl = parent.querySelector('.item-restaurant-info .item-restaurant-name');
            let priceEl = parent.querySelector('.item-restaurant-more .product-price > .current-price');
            let price = priceEl.textContent.replace(/[^0-9]+/g, '');
            if (nameEl && priceEl) {
                console.log(`Now adding ${nameEl.textContent} - ${priceEl.textContent}`);
                let res = trackClickAddFoodToCart(nameEl.textContent, price);
                console.log(res ? `Added ${nameEl.textContent}` : `Could not Add ${nameEl.textContent}`);
            }
        }
    } catch (e) {
        console.log('onAddFoodToCart: error: ', e);
    }
}

function isElement(element) {
    return element instanceof Element || element instanceof HTMLDocument;
}

function getCollection() {
    console.log('getCollection:');
    let req = new XMLHttpRequest();
    req.onreadystatechange = () => {
        if (req.readyState == XMLHttpRequest.DONE) {
            console.log('getCollection: resp: ', req.responseText);
            let collections = JSON.parse(req.responseText);
            collection = collections[0];
        }
    };
    req.open("GET", COLLECTION_URI, true);
    req.setRequestHeader("X-Master-Key", M_KEY);
    req.send();
}

async function trackClickAddFoodToCart(food, price) {
    if (!collection) {
        return;
    }

    /* add new record */
    let _food = food.replaceAll(/[\s]+/g, '');
    if (_food.length > 30) {
        _food = _food.slice(0, 30);
    }
    let user = currentUser || 'unknown';
    let binName = `${user}__${encodeURIComponent(_food)}__${price}`;
    let req = new XMLHttpRequest();
    req.onreadystatechange = async () => {
        if (req.readyState == XMLHttpRequest.DONE) {
            console.log("trackClickAddFoodToCart: resp: ", req.responseText);
        }
    };
    req.open("POST", BIN_URI, true);
    req.setRequestHeader("Content-Type", "application/json");
    req.setRequestHeader("X-Master-Key", M_KEY);
    req.setRequestHeader("X-Bin-Private", 'false');
    req.setRequestHeader("X-Bin-Name", '' + binName);
    req.setRequestHeader("X-Collection-Id", collection.record);
    let res = await req.send(JSON.stringify({
        "user": user,
        "food": food,
        "price": price
    }));
    return res && res.record;
}

/* === ./End local functions === */